# TaurineC

An open-source and fully conformant C compiler written in C++ using JUCE.

## Dialects

* ANSI-C/C89/C90
* C98
* C11
* C18

## Features

* SMALL! You can compile and execute C code everywhere, for example on rescue disks.
* FAST! TaurineC generates optimised x86 code. No byte code overhead. Compile, assemble and link about 7 times faster than `gcc -O0`.
* UNLIMITED! Any C dynamic library can be used directly.
* SAFE! TaurineC includes an optional memory and bound checker. Bound checked code can be mixed freely with standard code.
* Compile and execute C source directly. No linking or assembly necessary. Full C preprocessor included.
* C script supported : just add `#!/usr/local/bin/taurinec -run` at the first line of your C source, and execute it directly from the command line.

## License

TaurineC is licensed under GPLv3.

## Release Policy

### Who is "The Decider?"

[Joël R. Langlois](https://www.linkedin.com/in/jrlanglois/) is ultimately the decider and will answer all [5W2H](https://en.wikipedia.org/wiki/Five_Ws).

### Operations

The release frequency for major versions will depend on the scale of the features, meaning this project does not abide by forced cycles.

We use major.minor.build versioning and will tag the repository with such.

## Branch Policy

The `master` branch is the one where all contributions will be merged as released changes.
