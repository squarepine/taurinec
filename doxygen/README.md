# TaurineC API Reference

From here, you can generate an offline HTML version of the TaurineC API Reference.

## How To

1. Install [`doxygen`](https://www.stack.nl/~dimitri/doxygen/download.html)
2. `cd` into this directory on the command line
3. run `doxygen` (No additional arguments needed.)
4. `doxygen` will create a new subfolder `doc`.
5. Open `doc/index.html` in your browser to access the generated HTML documentation.
