#ifndef _TAURINEC_CTYPE_H_
#define _TAURINEC_CTYPE_H_

int isalnum(int c);
int isalpha(int c);
int iscntrl(int c);
int isdigit(int c);
int isgraph(int c);
int islower(int c);
int isprint(int c);
int ispunct(int c);
int isspace(int c);
int isupper(int c);
int isxdigit(int c);
int tolower(int c);
int toupper(int c);

#define _tolower(c) tolower (c);
#define _toupper(c) toupper (c);

#endif //_TAURINEC_CTYPE_H_
