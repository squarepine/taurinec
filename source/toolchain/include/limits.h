#ifndef _TAURINEC_LIMITS_H_
#define _TAURINEC_LIMITS_H_

#include <taurinec.h>

#define CHAR_MIN        0x00                            //< Mimimum char value
#define CHAR_MAX        0xff                            //< Maximum char value
#define SCHAR_MIN       0x80                            //< Minimum signed char value
#define SCHAR_MAX       0x7F                            //< Maximum signed char value
#define UCHAR_MAX       0xff                            //< Maximum unsigned char value
#define SHRT_MIN        0x8000                          //< Minimum (signed) short value
#define SHRT_MAX        0x7fff                          //< Maximum (signed) short value
#define USHRT_MAX       0xffff                          //< Maximum unsigned short value
#define INT_MIN         0x80000000                      //< Minimum (signed) int value
#define INT_MAX         0x7fffffff                      //< Maximum (signed) int value
#define UINT_MAX        0xffffffff                      //< Maximum unsigned int value
#define LONG_MIN        INT_MIN                         //< Minimum (signed) long value
#define LONG_MAX        INT_MAX                         //< Maximum (signed) long value
#define ULONG_MAX       UINT_MAX                        //< Maximum unsigned long value
#define LLONG_MAX       0x7fffffffffffffff              //< Maximum signed long long int value
#define LLONG_MIN       0x8000000000000000              //< Minimum signed long long int value
#define ULLONG_MAX      0xffffffffffffffff              //< Maximum unsigned long long int value

#define CHAR_BIT        8                               //< Number of bits in a char
#define MB_LEN_MAX      5                               //< Max. # bytes in multibyte char

#endif //_TAURINEC_LIMITS_H_
