#ifndef _TAURINEC_COMPLEX_H_
#define _TAURINEC_COMPLEX_H_

#include <taurinec.h>

#if ! __STDC_NO_COMPLEX__

#define _Imaginary_I
#define _Complex_I

#define imaginary _Imaginary
#define complex _Complex
#define I /* unspecified */

float complex CMPLXF (float real, float imag);
double complex CMPLX (double real, double imag);
long double complex CMPLXL (long double real, long double imag);

double cabs (double complex z);
double carg (double complex z);
double cimag (double complex z);
double complex cacos (double complex z);
double complex cacosh (double complex z);
double complex casin (double complex z);
double complex casinh (double complex z);
double complex catan (double complex z);
double complex catanh (double complex z);
double complex ccos (double complex z);
double complex ccosh (double complex z);
double complex cexp (double complex z);
double complex clog (double complex z);
double complex conj (double complex z);
double complex cpow (double complex x, double complex y);
double complex csin (double complex z);
double complex csinh (double complex z);
double complex csqrt (double complex z);
double complex ctan (double complex z);
double complex ctanh (double complex z);
double cproj (double complex z);
double creal (double complex z);
float cabsf (float complex z);
float cargf (float complex z);
float cimagf (float complex z);
float complex cacosf (float complex z);
float complex cacoshf (float complex z);
float complex casinf (float complex z);
float complex casinhf (float complex z);
float complex catanf (float complex z);
float complex catanhf (float complex z);
float complex ccosf (float complex z);
float complex ccoshf (float complex z);
float complex cexpf (float complex z);
float complex clogf (float complex z);
float complex conjf (float complex z);
float complex cpowf (float complex x, float complex y);
float complex csinf (float complex z);
float complex csinhf (float complex z);
float complex csqrtf (float complex z);
float complex ctanf (float complex z);
float complex ctanhf (float complex z);
float cprojf (float complex z);
float crealf (float complex z);
long double complex casinhl (long double complex z);

#define acos(z)
#define acosh(z)
#define asin(z)
#define asinh(z)
#define atan(z)
#define atanh(z)
#define carg(z)
#define cimag(z)
#define conj(z)
#define cos(z)
#define cosh(z)
#define cproj(z)
#define creal(z)
#define exp(z)
#define fabs(z)
#define log(z)
#define pow(x, y)
#define sin(z)
#define sinh(z)
#define sqrt(z)
#define tan(z)
#define tanh(z)

#endif //__STDC_NO_COMPLEX__

#endif //_TAURINEC_COMPLEX_H_
