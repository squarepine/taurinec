#ifndef _TAURINEC_LOCALE_H_
#define _TAURINEC_LOCALE_H_

#include <taurinec.h>
#include <taurinec_null.h>

#define LC_ALL          0
#define LC_COLLATE      1
#define LC_CTYPE        2
#define LC_MONETARY     3
#define LC_NUMERIC      4
#define LC_TIME         5
#define LC_MESSAGES     6

#define LC_MIN          LC_ALL
#define LC_MAX          LC_MESSAGES

/**
    https://code.woboq.org/qt5/include/locale.h.html
*/
typedef struct lconv
{
    char* decimal_point;
    char* thousands_sep;
    char* grouping;
    char* mon_decimal_point;
    char* mon_thousands_sep;
    char* mon_grouping;
    char* positive_sign;
    char* negative_sign;
    char* currency_symbol;
    char* int_curr_symbol;
    char int_frac_digits;
    char int_p_cs_precedes;
    char int_n_cs_precedes;
    char int_p_sep_by_space;
    char int_n_sep_by_space;
    char int_p_sign_posn;
    char int_n_sign_posn;
    char frac_digits;
    char p_cs_precedes;
    char p_sep_by_space;
    char n_cs_precedes;
    char n_sep_by_space;
    char p_sign_posn;
    char n_sign_posn;
} lconv;

extern lconv* localeconv();
extern char* setlocale (int category, const char* localeName);

#endif //_TAURINEC_LOCALE_H_
