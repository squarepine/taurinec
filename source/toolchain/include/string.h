#ifndef _TAURINEC_STRING_H_
#define _TAURINEC_STRING_H_

#include <errno.h>

#include <taurinec_null.h>
#include <taurinec_sizet.h>

char* strcat (char* destination, const char* source);
char* strchr (const char* source, int c);
char* strcpy (char* destination, const char* source);
char* strdup (const char* source);
char* strerror_r (int, char* source, size_t);
char* strerror (int);
char* strncat (char* destination, const char* source, size_t n);
char* strncpy (char* destination, const char* source, size_t n);
char* strpbrk (const char* source, const char* accept);
char* strrchr (const char* source, int c);
char* strsignal (int sig);
char* strstr (const char* haystack, const char* needle);
char* strtok_r (char* source, const char* delimiter, char** saveptr);
char* strtok (char* source, const char* delimiter);
int memcmp (const void* a, const void* b, size_t n);
int strcmp (const char* a, const char* b);
int strcoll (const char* source, const char* );
int strerror_r (int, char* source, size_t);
int strncmp (const char* source, const char* source, size_t n);
size_t strcspn (const char* source, const char* reject);
size_t strlcat (char* destination, const char* source, size_t n);
size_t strlcpy (char* destination, const char* source, size_t n);
size_t strlen (const char* );
size_t strspn (const char* source, const char* accept);
size_t strxfrm (char* destination, const char* source, size_t n);
void* memccpy (void* destination, const void* source, int c, size_t n);
void* memchr (const void* s, int c, size_t n);
void* memcpy (void* destination, const void* source, size_t n);
void* memmove (void* destination, const void* source, size_t n);
void* mempcpy (void* destination, const void* source, size_t n);
void* memset (void* , int c, size_t n);

//ISO/IEC WDTR 24731
errno_t strcat_s (char* destination, size_t n, const char* source);
errno_t strcpy_s (char* destination, size_t n, const char* source);

#endif //_TAURINEC_STRING_H_
