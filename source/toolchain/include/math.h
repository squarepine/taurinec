#ifndef _TAURINEC_MATH_H_
#define _TAURINEC_MATH_H_

/**
    https://pubs.opengroup.org/onlinepubs/009695399/basedefs/math.h.html
*/

//==============================================================================
#ifndef FLT_EVAL_METHOD
    #define FLT_EVAL_METHOD 0
#endif

#if FLT_EVAL_METHOD == 0
    typedef float float_t;
    typedef double double_t;
#elif FLT_EVAL_METHOD == 1
    typedef float float_t;
    typedef double double_t;
#elif FLT_EVAL_METHOD == 2
    typedef long double float_t;
    typedef long double double_t;
#else
    #error "TaurineC: Broken FLT_EVAL_METHOD macro."
#endif

//==============================================================================
extern int signgam;

//==============================================================================
#undef __HUGE_ENUF
#define __HUGE_ENUF  1e+300  // __HUGE_ENUF*__HUGE_ENUF must overflow

#define INFINITY   ((float) (__HUGE_ENUF * __HUGE_ENUF))
#define HUGE_VAL   ((double) INFINITY)
#define HUGE_VALF  ((float) INFINITY)
#define HUGE_VALL  ((long double) INFINITY)
#define NAN        ((float) (INFINITY * 0.0f))

//==============================================================================
#define FP_INFINITE         _INFCODE
#define FP_NAN              _NANCODE
#define FP_NORMAL           _FINITE
#define FP_SUBNORMAL        _DENORM
#define FP_ZERO             0
#define FP_FAST_FMA         0
#define FP_FAST_FMAF        0
#define FP_FAST_FMAL        0
#define _C2                 1  // 0 if not 2's complement
#define FP_ILOGB0           (-0x7fffffff - _C2)
#define FP_ILOGBNAN         0x7fffffff
#define MATH_ERRNO          1
#define MATH_ERREXCEPT      2
#define math_errhandling    (MATH_ERRNO | MATH_ERREXCEPT)

//==============================================================================
#define M_E         2.71828182845904523536   //< e
#define M_LOG2E     1.44269504088896340736   //< log2 (e)
#define M_LOG10E    0.434294481903251827651  //< log10 (e)
#define M_LN2       0.693147180559945309417  //< ln (2)
#define M_LN10      2.30258509299404568402   //< ln (10)
#define M_PI        3.14159265358979323846   //< pi
#define M_PI_2      1.57079632679489661923   //< pi / 2
#define M_PI_4      0.785398163397448309616  //< pi / 4
#define M_1_PI      0.318309886183790671538  //< 1 / pi
#define M_2_PI      0.636619772367581343076  //< 2 / pi
#define M_2_SQRTPI  1.12837916709551257390   //< 2 / sqrt (pi)
#define M_SQRT2     1.41421356237309504880   //< sqrt (2)
#define M_SQRT1_2   0.707106781186547524401  //< 1 / sqrt (2)

//==============================================================================
//These should be macros...
int fpclassify(real-floating x);
int isfinite(real-floating x);
int isinf(real-floating x);
int isnan(real-floating x);
int isnormal(real-floating x);
int signbit(real-floating x);
int isgreater(real-floating x, real-floating y);
int isgreaterequal(real-floating x, real-floating y);
int isless(real-floating x, real-floating y);
int islessequal(real-floating x, real-floating y);
int islessgreater(real-floating x, real-floating y);
int isunordered(real-floating x, real-floating y);

//==============================================================================
double acos (double);
double acosh (double);
double asin (double);
double asinh (double);
double atan (double);
double atan2 (double, double);
double atanh (double);
double cbrt (double);
double ceil (double);
double copysign (double, double);
double cos (double);
double cosh (double);
double erf (double);
double erfc (double);
double exp (double);
double exp2 (double);
double expm1 (double);
double fabs (double);
double fdim (double, double);
double floor (double);
double fma (double, double, double);
double fmax (double, double);
double fmin (double, double);
double fmod (double, double);
double frexp (double, int*);
double hypot (double, double);
double j0 (double);
double j1 (double);
double jn (int, double);
double ldexp (double, int);
double lgamma (double);
double log (double);
double log10 (double);
double log1p (double);
double log2 (double);
double logb (double);
double modf (double, double*);
double nan (const char*);
double nearbyint (double);
double nextafter (double, double);
double nexttoward (double, long double);
double pow (double, double);
double remainder (double, double);
double remquo (double, double, int*);
double rint (double);
double round (double);
double scalb (double, double);
double scalbln (double, long);
double scalbn (double, int);
double sin (double);
double sinh (double);
double sqrt (double);
double tan (double);
double tanh (double);
double tgamma (double);
double trunc (double);
double y0 (double);
double y1 (double);
double yn (int, double);
float acosf (float);
float acoshf (float);
float asinf (float);
float asinhf (float);
float atan2f (float, float);
float atanf (float);
float atanhf (float);
float cbrtf (float);
float ceilf (float);
float copysignf (float, float);
float cosf (float);
float coshf (float);
float erfcf (float);
float erff (float);
float exp2f (float);
float expf (float);
float expm1f (float);
float fabsf (float);
float fdimf (float, float);
float floorf (float);
float fmaf (float, float, float);
float fmaxf (float, float);
float fminf (float, float);
float fmodf (float, float);
float frexpf (float, int*);
float hypotf (float, float);
float ldexpf (float, int);
float lgammaf (float);
float log10f (float);
float log1pf (float);
float log2f (float);
float logbf (float);
float logf (float);
float modff (float, float*);
float nanf (const char*);
float nearbyintf (float);
float nextafterf (float, float);
float nexttowardf (float, long double);
float powf (float, float);
float remainderf (float, float);
float remquof (float, float, int*);
float rintf (float);
float roundf (float);
float scalblnf (float, long);
float scalbnf (float, int);
float sinf (float);
float sinhf (float);
float sqrtf (float);
float tanf (float);
float tanhf (float);
float tgammaf (float);
float truncf (float);
int ilogb (double);
int ilogbf (float);
int ilogbl (long double);
long  lrint (double);
long  lrintf (float);
long  lrintl (long double);
long  lround (double);
long  lroundf (float);
long  lroundl (long double);
long double acoshl (long double);
long double acosl (long double);
long double asinhl (long double);
long double asinl (long double);
long double atan2l (long double, long double);
long double atanhl (long double);
long double atanl (long double);
long double cbrtl (long double);
long double ceill (long double);
long double copysignl (long double, long double);
long double coshl (long double);
long double cosl (long double);
long double erfcl (long double);
long double erfl (long double);
long double exp2l (long double);
long double expl (long double);
long double expm1l (long double);
long double fabsl (long double);
long double fdiml (long double, long double);
long double floorl (long double);
long double fmal (long double, long double, long double);
long double fmaxl (long double, long double);
long double fminl (long double, long double);
long double fmodl (long double, long double);
long double frexpl (long double, int*);
long double hypotl (long double, long double);
long double ldexpl (long double, int);
long double lgammal (long double);
long double log10l (long double);
long double log1pl (long double);
long double log2l (long double);
long double logbl (long double);
long double logl (long double);
long double modfl (long double, long double*);
long double nanl (const char*);
long double nearbyintl (long double);
long double nextafterl (long double, long double);
long double nexttowardl (long double, long double);
long double powl (long double, long double);
long double remainderl (long double, long double);
long double remquol (long double, long double, int*);
long double rintl (long double);
long double roundl (long double);
long double scalblnl (long double, long);
long double scalbnl (long double, int);
long double sinhl (long double);
long double sinl (long double);
long double sqrtl (long double);
long double tanhl (long double);
long double tanl (long double);
long double tgammal (long double);
long double truncl (long double);
long long llrint (double);
long long llrintf (float);
long long llrintl (long double);
long long llround (double);
long long llroundf (float);
long long llroundl (long double);

#endif //_TAURINEC_MATH_H_
