#ifndef _TAURINEC_SYSTEM_CONFIG_H_
#define _TAURINEC_SYSTEM_CONFIG_H_

/*
    <assert.h>
    <complex.h>
    <ctype.h>
    <errno.h>
    <fenv.h>
    <float.h>
    <inttypes.h>
    <iso646.h>
    <limits.h>
    <locale.h>
    <math.h>
    <setjmp.h>
    <signal.h>
    <stdarg.h>
    <stdbool.h>
    <stddef.h>
    <stdint.h>
    <stdio.h>
    <stdlib.h>
    <string.h>
    <tgmath.h>
    <time.h>
    <wchar.h>
    <wctype.h>
*/

//==============================================================================
#if ! NDEBUG && (! _DEBUG || ! DEBUG)
    #undef NDEBUG
    #undef _DEBUG
    #undef DEBUG

    #define DEBUG 1
    #define _DEBUG 1
#endif

#ifndef TAURINEC_64BIT
    #define TAURINEC_64BIT 0
#endif

//==============================================================================
/** ANSI/ISO C Standard Predefined Macros */
/**
__LINE__
__FILE__
__DATE__
__TIME__
__func__
*/

#ifndef __OBJC__
    #define __OBJC__ 0
#endif

#ifndef __ASSEMBLER__
    #define __ASSEMBLER__ 0
#endif

#undef __STDC_HOSTED__
#define __STDC_HOSTED__ 1

#undef __STDC__
#define __STDC__ 1

#undef __STDC_VERSION__
#define __STDC_VERSION__ 201710L //< ISO/IEC 9899:2018

//==============================================================================
#ifndef __STDC_ANALYZABLE__
    #define __STDC_ANALYZABLE__ 1
#endif

#ifndef __STDC_LIB_EXT1__
    #define __STDC_LIB_EXT1__ 1
#endif

#ifndef __STDC_NO_THREADS__
    #define __STDC_NO_THREADS__ 0
#endif

#ifndef __STDC_NO_ATOMICS__
    #define __STDC_NO_ATOMICS__0
#endif

#ifndef __STDC_IEC_559__
    #define __STDC_IEC_559__ 1
#endif

#ifndef __STDC_IEC_559_COMPLEX__
    #define __STDC_IEC_559_COMPLEX__ 1
#endif

#ifndef __STDC_NO_COMPLEX__
    #define __STDC_NO_COMPLEX__ 0
#endif

#ifndef __STDC_NO_VLA__
    #define __STDC_NO_VLA__ 0
#endif

#endif //_TAURINEC_SYSTEM_CONFIG_H_
