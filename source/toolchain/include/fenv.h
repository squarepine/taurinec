#ifndef _TAURINEC_FENV_H_
#define _TAURINEC_FENV_H_

#include <math.h>

/** TODO:
    The FENV_ACCESS pragma provides a means to inform the implementation when an application might access the
    floating-point environment to test floating-point status flags or run under non-default floating-point control modes.
    The pragma shall occur either outside external declarations or preceding all explicit declarations
    and statements inside a compound statement. When outside external declarations, the pragma takes effect from
    its occurrence until another FENV_ACCESS pragma is encountered, or until the end of the translation unit.

    When inside a compound statement, the pragma takes effect from its occurrence until another FENV_ACCESS pragma
    is encountered (including within a nested compound statement), or until the end of the compound statement;
    at the end of a compound statement the state for the pragma is restored to its condition just before the compound statement.
    If this pragma is used in any other context, the behavior is undefined. If part of an application tests floating-point status flags,
    sets floating-point control modes, or runs under non-default mode settings, but was translated with the state for the FENV_ACCESS pragma off,
    the behavior is undefined. The default state (on or off) for the pragma is implementation-defined.

    (When execution passes from a part of the application translated with FENV_ACCESS off to a part translated with FENV_ACCESS on,
    the state of the floating-point status flags is unspecified and the floating-point control modes have their default settings.)

    For example:

    #include <fenv.h>
    void f(double x)
    {
        #pragma STDC FENV_ACCESS ON
        void g(double);
        void h(double);
        //do stuff...
        g(x + 1);
        h(x + 1);
        //do more stuff...
    }

    If the function g() might depend on status flags set as a side effect of the first x+1, or if the second x+1 might depend on control modes
    set as a side effect of the call to function g(), then the application shall contain an appropriately placed invocation as follows:
    #pragma STDC FENV_ACCESS ON
*/

#if ((math_errhandling & MATH_ERREXCEPT) != 0)
    #define FE_DIVBYZERO 1
    #define FE_INVALID 1
    #define FE_OVERFLOW  1
#endif

#define FE_TONEAREST    0x00000000
#define FE_UPWARD       0x00000200
#define FE_DOWNWARD     0x00000100
#define FE_TOWARDZERO   0x00000300
#define FE_INEXACT      0x00000001
#define FE_UNDERFLOW    0x00000002
#define FE_OVERFLOW     0x00000004
#define FE_DIVBYZERO    0x00000008
#define FE_INVALID      0x00000010

#define FE_ALL_EXCEPT   (FE_DIVBYZERO | FE_INEXACT | FE_INVALID | FE_OVERFLOW | FE_UNDERFLOW)

#define FE_DFL_ENV      (&_Fenv1)

typedef unsigned int fexcept_t;

/** This can literally be any kind of thing I want to be able to fuck with the CPU. */
typedef struct fenv_t
{
    unsigned long _Fe_ctl, _Fe_stat;
} fenv_t;

int feclearexcept (int);
int fegetexceptflag (fexcept_t*, int);
int feraiseexcept (int);
int fesetexceptflag (const fexcept_t*, int);
int fetestexcept (int);
int fegetround (void);
int fesetround (int);
int fegetenv (fenv_t*);
int feholdexcept (fenv_t*);
int fesetenv (const fenv_t*);
int feupdateenv (const fenv_t *);

#endif //_TAURINEC_FENV_H_
