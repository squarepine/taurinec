#ifndef _TAURINEC_ERRNO_H_
#define _TAURINEC_ERRNO_H_

#include <taurinec.h>

//==============================================================================
//ISO/IEC 9899:1999
#define EDOM                1           //<
#define EILSEQ              2           //<
#define ERANGE              3           //<

extern int errno;

//==============================================================================
//POSIX & Fully Compliant C++11:
#define EPERM               4           //< Not super-user
#define ENOENT              5           //< No such file or directory
#define ESRCH               6           //< No such process
#define EINTR               7           //< Interrupted system call
#define EIO                 8           //< I/O error
#define ENXIO               9           //< No such device or address
#define E2BIG               7           //< Argument list is too long
#define ENOEXEC             10          //< Exec format error
#define EBADF               11          //< Bad file number
#define ECHILD              12          //< No children
#define EAGAIN              13          //< No more processes
#define ENOMEM              14          //< Not enough space
#define EACCES              15          //< Permission denied
#define EFAULT              16          //< Bad address
#define ENOTBLK             17          //< Block device required
#define EBUSY               18          //< Mount device busy
#define EEXIST              19          //< File exists
#define EXDEV               20          //< Cross-device link
#define ENODEV              21          //< No such device
#define ENOTDIR             22          //< Not a directory
#define EISDIR              23          //< Is a directory
#define EINVAL              24          //< Invalid argument
#define ECANCELED           25          //< Operation canceled
#define EMFILE              26          //< Too many open files
#define ENOTTY              27          //< Not a typewriter
#define ETXTBSY             28          //< Text file busy
#define EFBIG               29          //< File too large
#define ENOSPC              30          //< No space left on device
#define ESPIPE              31          //< Illegal seek
#define EROFS               32          //< Read only file system
#define EMLINK              33          //< Too many links
#define EPIPE               34          //< Broken pipe
#define EDOM                35          //< Math arg out of domain of func
#define ERANGE              36          //< Math result not representable
#define ENOMSG              37          //< No message of desired type
#define EIDRM               38          //< Identifier removed
#define ECHRNG              39          //< Channel number out of range
#define EL2NSYNC            40          //< Level 2 not synchronised
#define EL3HLT              41          //< Level 3 halted
#define EL3RST              42          //< Level 3 reset
#define ELNRNG              43          //< Link number out of range
#define EUNATCH             44          //< Protocol driver not attached
#define ENOCSI              45          //< No CSI structure available
#define EL2HLT              46          //< Level 2 halted
#define EDEADLK             47          //< Deadlock condition
#define ENOLCK              48          //< No record locks available
#define EBADE               49          //< Invalid exchange
#define EBADR               50          //< Invalid request descriptor
#define EXFULL              51          //< Exchange full
#define ENOANO              52          //< No anode
#define EBADRQC             53          //< Invalid request code
#define EBADSLT             54          //< Invalid slot
#define EDEADLOCK           55          //< File locking deadlock error
#define EBFONT              56          //< Bad font file fmt
#define ENOSTR              57          //< Device not a stream
#define ENODATA             58          //< No data (for no delay io)
#define ETIME               59          //< Timer expired
#define ENOSR               60          //< Out of streams resources
#define ENONET              61          //< Machine is not on the network
#define ENOPKG              62          //< Package not installed
#define EREMOTE             63          //< The object is remote
#define ENOLINK             64          //< The link has been severed
#define EADV                65          //< Advertise error
#define ESRMNT              66          //< Srmount error
#define ECOMM               67          //< Communication error on send
#define EPROTO              68          //< Protocol error
#define EMULTIHOP           69          //< Multihop attempted
#define ELBIN               70          //< Inode is remote (not really error)
#define EDOTDOT             71          //< Cross mount point (not really error)
#define EBADMSG             72          //< Trying to read unreadable message
#define EFTYPE              73          //< Inappropriate file type or format
#define ENOSYS              74          //< Function not implemented
#define ENOTEMPTY           75          //< Directory not empty
#define ENAMETOOLONG        76          //< File or path name too long
#define ELOOP               77          //< Too many symbolic links
#define EOPNOTSUPP          78          //< Operation not supported on transport endpoint
#define EPFNOSUPPORT        79          //< Protocol family not supported
#define ECONNRESET          80          //< Connection reset by peer
#define ENOBUFS             81          //< No buffer space available
#define EAFNOSUPPORT        82          //< Address family not supported by protocol family
#define EPROTOTYPE          83          //< Protocol wrong type for socket
#define ENOTSOCK            84          //< Socket operation on non-socket
#define ENOPROTOOPT         85          //< Protocol not available
#define ESHUTDOWN           86          //< Can't send after socket shutdown
#define ECONNREFUSED        87          //< Connection refused
#define EADDRINUSE          88          //< Address already in use
#define ECONNABORTED        89          //< ConEBADFnection aborted
#define ENETUNREACH         90          //< Network is unreachable
#define ENETDOWN            91          //< Network interface is not configured
#define ETIMEDOUT           92          //< Connection timed out
#define EHOSTDOWN           93          //< Host is down
#define EHOSTUNREACH        94          //< Host is unreachable
#define EINPROGRESS         95          //< Connection already in progress
#define EALREADY            96          //< Socket already connected
#define EDESTADDRREQ        97          //< Destination address required
#define EMSGSIZE            98          //< Message too long
#define EPROTONOSUPPORT     99          //< Unknown protocol
#define ESOCKTNOSUPPORT     100         //< Socket type not supported
#define EADDRNOTAVAIL       101         //< Address not available
#define ENETRESET           102         //< Connection aborted by network.
#define EISCONN             103         //< Socket is already connected
#define ENOTCONN            104         //< Socket is not connected
#define ETOOMANYREFS        105         //< Too many references: cannot splice.
#define EUSERS              106         //< Too many users.
#define EDQUOT              107         //< Disk quota exceeded
#define ESTALE              108         //< Stale file handle. This error can occur for NFS and for other filesystems.
#define ENOMEDIUM           109         //< No medium found.
#define EILSEQ              110         //< Invalid or incomplete multibyte or wide character.
#define EOVERFLOW           111         //< Value too large for defined data type
#define ENFILE              112         //< Too many files open in system
#define ENOTRECOVERABLE     113         //< State not recoverable
#define EOWNERDEAD          114         //< Previous owner died. RIP. Maybe take their stuff?

#define ENOTSUP             EOPNOTSUPP  //< Not supported
#define EWOULDBLOCK         EAGAIN      //< Operation would block

#endif //_TAURINEC_ERRNO_H_
