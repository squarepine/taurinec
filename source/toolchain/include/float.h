#ifndef _TAURINEC_FLOAT_H_
#define _TAURINEC_FLOAT_H_

#define _DBL_RADIX          2                       //< Exponent radix
#define _LDBL_RADIX         _DBL_RADIX              //< Exponent radix
#define DBL_DECIMAL_DIG     17                      //< Number of decimal digits of rounding precision
#define DBL_DIG             15                      //< Number of decimal digits of precision
#define DBL_EPSILON         2.2204460492503131e-016 //< Smallest such that 1.0+DBL_EPSILON != 1.0
#define DBL_HAS_SUBNORM     1                       //< C11 5.2.4.2.2: Type does support subnormal numbers
#define DBL_MANT_DIG        53                      //< Number of bits in mantissa
#define DBL_MAX             1.7976931348623158e+308 //< Max value
#define DBL_MAX_10_EXP      308                     //< Max decimal exponent
#define DBL_MAX_EXP         1024                    //< Max binary exponent
#define DBL_MIN             2.2250738585072014e-308 //< Min positive value
#define DBL_MIN_10_EXP      (-307)                  //< Min decimal exponent
#define DBL_MIN_EXP         (-1021)                 //< Min binary exponent
#define DBL_TRUE_MIN        4.9406564584124654e-324 //< C11 5.2.4.2.2: Min positive value

#define FLT_DECIMAL_DIG     9                       //< Number of decimal digits of rounding precision
#define FLT_DIG             6                       //< Number of decimal digits of precision
#define FLT_EPSILON         1.192092896e-07F        //< Smallest such that 1.0+FLT_EPSILON != 1.0
#define FLT_GUARD           0
#define FLT_HAS_SUBNORM     1                       //< C11 5.2.4.2.2: Type does support subnormal numbers
#define FLT_MANT_DIG        24                      //< Number of bits in mantissa
#define FLT_MAX             3.402823466e+38F        //< Max value
#define FLT_MAX_10_EXP      38                      //< Max decimal exponent
#define FLT_MAX_EXP         128                     //< Max binary exponent
#define FLT_MIN             1.175494351e-38F        //< Min normalized positive value
#define FLT_MIN_10_EXP      (-37)                   //< Min decimal exponent
#define FLT_MIN_EXP         (-125)                  //< Min binary exponent
#define FLT_NORMALIZE       0
#define FLT_RADIX           2                       //< Exponent radix
#define FLT_TRUE_MIN        1.401298464e-45F        //< C11 5.2.4.2.2: Min positive value

#define LDBL_DIG            DBL_DIG                 //< Number of decimal digits of precision
#define LDBL_EPSILON        DBL_EPSILON             //< Smallest such that 1.0+LDBL_EPSILON != 1.0
#define LDBL_HAS_SUBNORM    DBL_HAS_SUBNORM         //< C11 5.2.4.2.2: Type does support subnormal numbers
#define LDBL_MANT_DIG       DBL_MANT_DIG            //< Number of bits in mantissa
#define LDBL_MAX            DBL_MAX                 //< Max value
#define LDBL_MAX_10_EXP     DBL_MAX_10_EXP          //< Max decimal exponent
#define LDBL_MAX_EXP        DBL_MAX_EXP             //< Max binary exponent
#define LDBL_MIN            DBL_MIN                 //< Min normalized positive value
#define LDBL_MIN_10_EXP     DBL_MIN_10_EXP          //< Min decimal exponent
#define LDBL_MIN_EXP        DBL_MIN_EXP             //< Min binary exponent
#define LDBL_TRUE_MIN       DBL_TRUE_MIN            //< C11 5.2.4.2.2: Min positive value

#define DECIMAL_DIG         DBL_DECIMAL_DIG
#define FLT_EVAL_METHOD     0                       //< C11 5.2.4.2.2: Evaluates all operations and constants just to the precision of the type.
#define FLT_RADIX           2
#define FLT_ROUNDS          1                       //< C11 5.2.4.2.2: Rounds to nearest.

#endif //_TAURINEC_FLOAT_H_
