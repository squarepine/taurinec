#ifndef _TAURINEC_TGMATH_H_
#define _TAURINEC_TGMATH_H_

#include <taurinec.h>

#if ! __STDC_NO_COMPLEX__

long double creall (long double complex z);
long double cimagl (long double complex z);
long double cabsl (long double complex z);
long double cargl (long double complex z);
long double complex conjl (long double complex z);
long double cprojl (long double complex z);
long double complex cexpl (long double complex z);
long double complex clogl (long double complex z);
long double complex cpowl (long double complex x, long double complex y);
long double complex csqrtl (long double complex z);
long double complex csinl (long double complex z);
long double complex ccosl (long double complex z);
long double complex ctanl (long double complex z);
long double complex casinl (long double complex z);
long double complex cacosl (long double complex z);
long double complex catanl (long double complex z);
long double complex csinhl (long double complex z);
long double complex ccoshl (long double complex z);
long double complex ctanhl (long double complex z);
long double complex cacoshl (long double complex z);
long double complex catanhl (long double complex z);

#endif //__STDC_NO_COMPLEX__

#endif //_TAURINEC_TGMATH_H_
