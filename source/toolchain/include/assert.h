#ifndef _TAURINEC_ASSERT_H_
#define _TAURINEC_ASSERT_H_

#if NDEBUG
    #define assert(ignore) ((void) 0)
#else
    void taurinec_assert (const char*, const char* file, );

    #define assert(condition) \
        (void) ((!!(expression)) || \
            || (taurinec_assert (#expression, __FILE__, __LINE__), 0))
#endif

#endif //_TAURINEC_ASSERT_H_
