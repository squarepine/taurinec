#ifndef _TAURINEC_STD_BOOL_H_
#define _TAURINEC_STD_BOOL_H_

#define bool                            _Bool
#define true                            1
#define false                           0
#define __bool_true_false_are_defined   1

#endif //_TAURINEC_STD_BOOL_H_
