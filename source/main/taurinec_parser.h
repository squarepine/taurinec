#ifndef SQUAREPINE_TAURINEC_PARSER_H
#define SQUAREPINE_TAURINEC_PARSER_H

#include "JuceHeader.h"

#define TAURINEC_OPERATORS(X) \
    X (semicolon,           ";")   X (dot,          ".")       X (comma,        ",") \
    X (openParen,           "(")   X (closeParen,   ")")       X (openBrace,    "{")    X (closeBrace, "}") \
    X (openBracket,         "[")   X (closeBracket, "]")       X (colon,        ":")    X (question,   "?") \
    X (equals,              "==")  X (assign,       "=")       X (notEquals,    "!=")   X (logicalNot,   "!") \
    X (plusEquals,          "+=")  X (plusplus,     "++")      X (plus,         "+") \
    X (minusEquals,         "-=")  X (minusminus,   "--")      X (minus,        "-") \
    X (timesEquals,         "*=")  X (times,        "*")       X (divideEquals, "/=")   X (divide,     "/") \
    X (moduloEquals,        "%=")  X (modulo,       "%")       X (xorEquals,    "^=")   X (bitwiseXor, "^") \
    X (andEquals,           "&=")  X (logicalAnd,   "&&")      X (bitwiseAnd,   "&") \
    X (orEquals,            "|=")  X (logicalOr,    "||")      X (bitwiseOr,    "|") \
    X (lessThan,            "<")   X (greaterThan,  ">")       X (greaterThanOrEqual, ">=") \
    X (leftShiftEquals,     "<<=") X (lessThanOrEqual,  "<=")  X (leftShift,    "<<") \
    X (rightShiftUnsigned,  ">>>") X (rightShiftEquals, ">>=") X (rightShift,   ">>")

#define TAURINEC_C89_KEYWORDS(X) \
    X (do) X (if) X (for) X (int) \
    X (auto) X (case) X (char) X (else) \
    X (enum) X (goto) X (long) X (void) \
    X (break) X (const) X (float) X (short) \
    X (union) X (while) X (double) X (extern) \
    X (return) X (signed) X (sizeof) X (static) \
    X (struct) X (switch) X (default) X (typedef) \
    X (continue) X (register) X (unsigned) X (volatile)

#define TAURINEC_C99_KEYWORDS(X) \
    X (_Bool) X (_Complex) \
    X (_Imaginary) X (inline) X (restrict)

#define TAURINEC_C11_KEYWORDS(X) \
    X (_Alignas) X (_Alignof) X (_Atomic) \
    X (_Generic) X (_Noreturn) X (_Static_assert) X (_Thread_local)

namespace TokenTypes
{
    #define TAURINEC_DECLARE_TOKEN(name) \
        static const char* const name##_ = #name;

    TAURINEC_C89_KEYWORDS (TAURINEC_DECLARE_TOKEN)
    TAURINEC_C99_KEYWORDS (TAURINEC_DECLARE_TOKEN)
    TAURINEC_C11_KEYWORDS (TAURINEC_DECLARE_TOKEN)
    #undef TAURINEC_DECLARE_TOKEN

    #define TAURINEC_DECLARE_TOKEN(name, str) \
        static const char* const name = str;

    TAURINEC_OPERATORS (TAURINEC_DECLARE_TOKEN)
    TAURINEC_DECLARE_TOKEN (eof,        "$eof")
    TAURINEC_DECLARE_TOKEN (literal,    "$literal")
    TAURINEC_DECLARE_TOKEN (identifier, "$identifier")

    #undef TAURINEC_DECLARE_TOKEN
}

//==============================================================================
/** Class containing some basic functions for simple tokenising of C code. */
struct TaurineCTokeniserFunctions
{
    static bool isIdentifierStart (juce_wchar c) noexcept
    {
        return CharacterFunctions::isLetter (c) || c == '_' || c == '@';
    }

    static bool isIdentifierBody (juce_wchar c) noexcept
    {
        return CharacterFunctions::isLetterOrDigit (c) || c == '_' || c == '@';
    }

    static bool isDecimalDigit (juce_wchar c) noexcept
    {
        return c >= '0' && c <= '9';
    }

    static bool isHexDigit (juce_wchar c) noexcept
    {
        return (c >= '0' && c <= '9')
            || (c >= 'a' && c <= 'f')
            || (c >= 'A' && c <= 'F');
    }

    static bool isOctalDigit (juce_wchar c) noexcept
    {
        return c >= '0' && c <= '7';
    }

    static bool isReservedKeyword (String::CharPointerType token, int tokenLength) noexcept
    {
        static const char* const keywords2Char[] =
        { "do", "if", "or", nullptr };

        static const char* const keywords3Char[] =
        { "and", "asm", "for", "int", "new", "not", "try", "xor", nullptr };

        static const char* const keywords4Char[] =
        { "auto", "bool", "case", "char", "else", "enum", "goto",
              "long", "this", "true", "void", nullptr };

        static const char* const keywords5Char[] =
        { "bitor", "break", "catch", "class", "compl", "const", "false", "final",
              "float", "or_eq", "short", "throw", "union", "using", "while", nullptr };

        static const char* const keywords6Char[] =
        { "and_eq", "bitand", "delete", "double", "export", "extern", "friend",
              "import", "inline", "module", "not_eq", "public", "return", "signed",
              "sizeof", "static", "struct", "switch", "typeid", "xor_eq", nullptr };

        static const char* const keywords7Char[] =
        { "__cdecl", "_Pragma", "alignas", "alignof", "concept", "default",
              "mutable", "nullptr", "private", "typedef", "uint8_t", "virtual",
              "wchar_t", nullptr };

        static const char* const keywordsOther[] =
        { "@class", "@dynamic", "@end", "@implementation", "@interface", "@public",
              "@private", "@protected", "@property", "@synthesize", "__fastcall", "__stdcall",
              "atomic_cancel", "atomic_commit", "atomic_noexcept", "char16_t", "char32_t",
              "co_await", "co_return", "co_yield", "const_cast", "constexpr", "continue",
              "decltype", "dynamic_cast", "explicit", "namespace", "noexcept", "operator", "override",
              "protected", "register", "reinterpret_cast", "requires", "static_assert",
              "static_cast", "synchronized", "template", "thread_local", "typename", "unsigned",
              "volatile", nullptr };

        const char* const* k;

        switch (tokenLength)
        {
            case 2: k = keywords2Char; break;
            case 3: k = keywords3Char; break;
            case 4: k = keywords4Char; break;
            case 5: k = keywords5Char; break;
            case 6: k = keywords6Char; break;
            case 7: k = keywords7Char; break;

            default:
                if (tokenLength < 2 || tokenLength > 16)
                    return false;

                k = keywordsOther;
                break;
        }

        for (int i = 0; k[i] != nullptr; ++i)
            if (token.compare (CharPointer_ASCII (k[i])) == 0)
                return true;

        return false;
    }

    template<typename Iterator>
    static int parseIdentifier (Iterator& source) noexcept
    {
        int tokenLength = 0;
        String::CharPointerType::CharType possibleIdentifier[100];
        String::CharPointerType possible (possibleIdentifier);

        while (isIdentifierBody (source.peekNextChar()))
        {
            const auto c = source.nextChar();

            if (tokenLength < 20)
                possible.write (c);

            ++tokenLength;
        }

        if (tokenLength > 1 && tokenLength <= 16)
        {
            possible.writeNull();

            if (isReservedKeyword (String::CharPointerType (possibleIdentifier), tokenLength))
                return TaurineCTokeniserFunctions::tokenType_keyword;
        }

        return TaurineCTokeniserFunctions::tokenType_identifier;
    }

    template<typename Iterator>
    static bool skipNumberSuffix (Iterator& source)
    {
        auto c = source.peekNextChar();

        if (c == 'l' || c == 'L' || c == 'u' || c == 'U')
            source.skip();

        if (CharacterFunctions::isLetterOrDigit (source.peekNextChar()))
            return false;

        return true;
    }

    template<typename Iterator>
    static bool parseHexLiteral (Iterator& source) noexcept
    {
        if (source.peekNextChar() == '-')
            source.skip();

        if (source.nextChar() != '0')
            return false;

        auto c = source.nextChar();

        if (c != 'x' && c != 'X')
            return false;

        int numDigits = 0;

        while (isHexDigit (source.peekNextChar()))
        {
            ++numDigits;
            source.skip();
        }

        if (numDigits == 0)
            return false;

        return skipNumberSuffix (source);
    }

    template<typename Iterator>
    static bool parseOctalLiteral (Iterator& source) noexcept
    {
        if (source.peekNextChar() == '-')
            source.skip();

        if (source.nextChar() != '0')
            return false;

        if (! isOctalDigit (source.nextChar()))
            return false;

        while (isOctalDigit (source.peekNextChar()))
            source.skip();

        return skipNumberSuffix (source);
    }

    template<typename Iterator>
    static bool parseDecimalLiteral (Iterator& source) noexcept
    {
        if (source.peekNextChar() == '-')
            source.skip();

        int numChars = 0;
        while (isDecimalDigit (source.peekNextChar()))
        {
            ++numChars;
            source.skip();
        }

        if (numChars == 0)
            return false;

        return skipNumberSuffix (source);
    }

    template<typename Iterator>
    static bool parseFloatLiteral (Iterator& source) noexcept
    {
        if (source.peekNextChar() == '-')
            source.skip();

        int numDigits = 0;

        while (isDecimalDigit (source.peekNextChar()))
        {
            source.skip();
            ++numDigits;
        }

        const bool hasPoint = source.peekNextChar() == '.';

        if (hasPoint)
        {
            source.skip();

            while (isDecimalDigit (source.peekNextChar()))
            {
                source.skip();
                ++numDigits;
            }
        }

        if (numDigits == 0)
            return false;

        auto c = source.peekNextChar();
        bool hasExponent = (c == 'e' || c == 'E');

        if (hasExponent)
        {
            source.skip();
            c = source.peekNextChar();

            if (c == '+' || c == '-')
                source.skip();

            int numExpDigits = 0;

            while (isDecimalDigit (source.peekNextChar()))
            {
                source.skip();
                ++numExpDigits;
            }

            if (numExpDigits == 0)
                return false;
        }

        c = source.peekNextChar();

        if (c == 'f' || c == 'F')
            source.skip();
        else if (! (hasExponent || hasPoint))
            return false;

        return true;
    }

    template<typename Iterator>
    static int parseNumber (Iterator& source)
    {
        const Iterator original (source);

        if (parseFloatLiteral (source))    return TaurineCTokeniserFunctions::tokenType_float;
        source = original;

        if (parseHexLiteral (source))      return TaurineCTokeniserFunctions::tokenType_integer;
        source = original;

        if (parseOctalLiteral (source))    return TaurineCTokeniserFunctions::tokenType_integer;
        source = original;

        if (parseDecimalLiteral (source))  return TaurineCTokeniserFunctions::tokenType_integer;
        source = original;

        return TaurineCTokeniserFunctions::tokenType_error;
    }

    template<typename Iterator>
    static void skipQuotedString (Iterator& source) noexcept
    {
        auto quote = source.nextChar();

        for (;;)
        {
            auto c = source.nextChar();

            if (c == quote || c == 0)
                break;

            if (c == '\\')
                source.skip();
        }
    }

    template<typename Iterator>
    static void skipComment (Iterator& source) noexcept
    {
        bool lastWasStar = false;

        for (;;)
        {
            auto c = source.nextChar();

            if (c == 0 || (c == '/' && lastWasStar))
                break;

            lastWasStar = (c == '*');
        }
    }

    template<typename Iterator>
    static void skipPreprocessorLine (Iterator& source) noexcept
    {
        bool lastWasBackslash = false;

        for (;;)
        {
            auto c = source.peekNextChar();

            if (c == '"')
            {
                skipQuotedString (source);
                continue;
            }

            if (c == '/')
            {
                Iterator next (source);
                next.skip();
                auto c2 = next.peekNextChar();

                if (c2 == '/' || c2 == '*')
                    return;
            }

            if (c == 0)
                break;

            if (c == '\n' || c == '\r')
            {
                source.skipToEndOfLine();

                if (lastWasBackslash)
                    skipPreprocessorLine (source);

                break;
            }

            lastWasBackslash = (c == '\\');
            source.skip();
        }
    }

    template<typename Iterator>
    static void skipIfNextCharMatches (Iterator& source, juce_wchar c) noexcept
    {
        if (source.peekNextChar() == c)
            source.skip();
    }

    template<typename Iterator>
    static void skipIfNextCharMatches (Iterator& source, juce_wchar c1, juce_wchar c2) noexcept
    {
        const auto c = source.peekNextChar();

        if (c == c1 || c == c2)
            source.skip();
    }

    template<typename Iterator>
    static int readNextToken (Iterator& source)
    {
        source.skipWhitespace();
        const auto firstChar = source.peekNextChar();

        switch (firstChar)
        {
            case 0:
            break;

            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
            case '.':
            {
                auto result = parseNumber (source);

                if (result == TaurineCTokeniserFunctions::tokenType_error)
                {
                    source.skip();

                    if (firstChar == '.')
                        return TaurineCTokeniserFunctions::tokenType_punctuation;
                }

                return result;
            }

            case ',': case ';': case ':':
                source.skip();
                return TaurineCTokeniserFunctions::tokenType_punctuation;

            case '(': case ')':
            case '{': case '}':
            case '[': case ']':
                source.skip();
                return TaurineCTokeniserFunctions::tokenType_bracket;

            case '"':
            case '\'':
                skipQuotedString (source);
                return TaurineCTokeniserFunctions::tokenType_string;

            case '+':
                source.skip();
                skipIfNextCharMatches (source, '+', '=');
                return TaurineCTokeniserFunctions::tokenType_operator;

            case '-':
            {
                source.skip();
                auto result = parseNumber (source);

                if (result == TaurineCTokeniserFunctions::tokenType_error)
                {
                    skipIfNextCharMatches (source, '-', '=');
                    return TaurineCTokeniserFunctions::tokenType_operator;
                }

                return result;
            }

            case '*': case '%':
            case '=': case '!':
                source.skip();
                skipIfNextCharMatches (source, '=');
                return TaurineCTokeniserFunctions::tokenType_operator;

            case '/':
            {
                source.skip();
                auto nextChar = source.peekNextChar();

                if (nextChar == '/')
                {
                    source.skipToEndOfLine();
                    return TaurineCTokeniserFunctions::tokenType_comment;
                }

                if (nextChar == '*')
                {
                    source.skip();
                    skipComment (source);
                    return TaurineCTokeniserFunctions::tokenType_comment;
                }

                if (nextChar == '=')
                    source.skip();

                return TaurineCTokeniserFunctions::tokenType_operator;
            }

            case '?':
            case '~':
                source.skip();
                return TaurineCTokeniserFunctions::tokenType_operator;

            case '<': case '>':
            case '|': case '&': case '^':
                source.skip();
                skipIfNextCharMatches (source, firstChar);
                skipIfNextCharMatches (source, '=');
                return TaurineCTokeniserFunctions::tokenType_operator;

            case '#':
                skipPreprocessorLine (source);
                return TaurineCTokeniserFunctions::tokenType_preprocessor;

            default:
                if (isIdentifierStart (firstChar))
                    return parseIdentifier (source);

                source.skip();
            break;
        }

        return TaurineCTokeniserFunctions::tokenType_error;
    }

    /** A class that can be passed to the TaurineCTokeniserFunctions functions in order to parse a String.
    */
    struct StringIterator
    {
        StringIterator (const String& s) noexcept            : t (s.getCharPointer()) {}
        StringIterator (String::CharPointerType s) noexcept  : t (s) {}

        juce_wchar nextChar() noexcept      { if (isEOF()) return 0; ++numChars; return t.getAndAdvance(); }
        juce_wchar peekNextChar() noexcept  { return *t; }
        void skip() noexcept                { if (! isEOF()) { ++t; ++numChars; } }
        void skipWhitespace() noexcept      { while (t.isWhitespace()) skip(); }
        void skipToEndOfLine() noexcept     { while (*t != '\r' && *t != '\n' && *t != 0) skip(); }
        bool isEOF() const noexcept         { return t.isEmpty(); }

        String::CharPointerType t;
        int numChars = 0;
    };

    //==============================================================================
    /** Takes a UTF8 string and writes it to a stream using standard C escape
        sequences for any non-ASCII bytes.

        Although not strictly a tokenising function, this is still a function that
        often comes in handy when working with C code!

        Note that addEscapeChars() is easier to use than this function if you're working with Strings.

        @see addEscapeChars
    */
    static void writeEscapeChars (OutputStream& out, const char* utf8, int numBytesToRead,
                                  int maxCharsOnLine, bool breakAtNewLines,
                                  bool replaceSingleQuotes, bool allowStringBreaks)
    {
        int charsOnLine = 0;
        bool lastWasHexEscapeCode = false;
        bool trigraphDetected = false;

        for (int i = 0; i < numBytesToRead || numBytesToRead < 0; ++i)
        {
            auto c = (uint8) utf8[i];
            bool startNewLine = false;

            switch (c)
            {
                case '\t': out << "\\t";  trigraphDetected = false; lastWasHexEscapeCode = false; charsOnLine += 2; break;
                case '\r': out << "\\r";  trigraphDetected = false; lastWasHexEscapeCode = false; charsOnLine += 2; break;
                case '\n': out << "\\n";  trigraphDetected = false; lastWasHexEscapeCode = false; charsOnLine += 2; startNewLine = breakAtNewLines; break;
                case '\\': out << "\\\\"; trigraphDetected = false; lastWasHexEscapeCode = false; charsOnLine += 2; break;
                case '\"': out << "\\\""; trigraphDetected = false; lastWasHexEscapeCode = false; charsOnLine += 2; break;

                case '?':
                    if (trigraphDetected)
                    {
                        out << "\\?";
                        ++charsOnLine;
                        trigraphDetected = false;
                    }
                    else
                    {
                        out << "?";
                        trigraphDetected = true;
                    }

                    lastWasHexEscapeCode = false;
                    ++charsOnLine;
                    break;

                case 0:
                    if (numBytesToRead < 0)
                        return;

                    out << "\\0";
                    lastWasHexEscapeCode = true;
                    trigraphDetected = false;
                    charsOnLine += 2;
                    break;

                case '\'':
                    if (replaceSingleQuotes)
                    {
                        out << "\\\'";
                        lastWasHexEscapeCode = false;
                        trigraphDetected = false;
                        charsOnLine += 2;
                        break;
                    }

                    // deliberate fall-through...

                default:
                    if (c >= 32 && c < 127 && ! (lastWasHexEscapeCode  // (have to avoid following a hex escape sequence with a valid hex digit)
                                                   && CharacterFunctions::getHexDigitValue (c) >= 0))
                    {
                        out << (char) c;
                        lastWasHexEscapeCode = false;
                        trigraphDetected = false;
                        ++charsOnLine;
                    }
                    else if (allowStringBreaks && lastWasHexEscapeCode && c >= 32 && c < 127)
                    {
                        out << "\"\"" << (char) c;
                        lastWasHexEscapeCode = false;
                        trigraphDetected = false;
                        charsOnLine += 3;
                    }
                    else
                    {
                        out << (c < 16 ? "\\x0" : "\\x") << String::toHexString ((int) c);
                        lastWasHexEscapeCode = true;
                        trigraphDetected = false;
                        charsOnLine += 4;
                    }

                    break;
            }

            if ((startNewLine || (maxCharsOnLine > 0 && charsOnLine >= maxCharsOnLine))
                && (numBytesToRead < 0 || i < numBytesToRead - 1))
            {
                charsOnLine = 0;
                out << "\"" << newLine << "\"";
                lastWasHexEscapeCode = false;
            }
        }
    }

    /** Takes a string and returns a version of it where standard C++ escape sequences have been
        used to replace any non-ascii bytes.

        Although not strictly a tokenising function, this is still a function that often comes in
        handy when working with C++ code!

        @see writeEscapeChars
    */
    static String addEscapeChars (const String& s)
    {
        MemoryOutputStream mo;
        writeEscapeChars (mo, s.toRawUTF8(), -1, -1, false, true, true);
        return mo.toString();
    }
};

//==============================================================================
struct TaurineCParserHelpers
{
    static bool parseHexInt (const String& text, int64& result)
    {
        TaurineCTokeniserFunctions::StringIterator i (text);

        if (TaurineCTokeniserFunctions::parseHexLiteral (i))
        {
            result = text.fromFirstOccurrenceOf ("x", false, true).getHexValue64();
            return true;
        }

        return false;
    }

    static bool parseOctalInt (const String& text, int64& result)
    {
        TaurineCTokeniserFunctions::StringIterator it (text);

        if (TaurineCTokeniserFunctions::parseOctalLiteral (it))
        {
            result = 0;

            for (int i = 0; i < text.length(); ++i)
            {
                const int digit = text[i] - '0';

                if (digit < 0 || digit > 7)
                    break;

                result = result * 8 + digit;
            }

            return true;
        }

        return false;
    }

    static bool parseDecimalInt (const String& text, int64& result)
    {
        TaurineCTokeniserFunctions::StringIterator i (text);

        if (TaurineCTokeniserFunctions::parseDecimalLiteral (i))
        {
            result = text.getLargeIntValue();
            return true;
        }

        return false;
    }

    static bool parseInt (const String& text, int64& result)
    {
        return parseHexInt (text, result)
            || parseOctalInt (text, result)
            || parseDecimalInt (text, result);
    }

    static bool parseFloat (const String& text, double& result)
    {
        TaurineCTokeniserFunctions::StringIterator i (text);

        if (TaurineCTokeniserFunctions::parseFloatLiteral (i))
        {
            result = text.getDoubleValue();
            return true;
        }

        return false;
    }

    static int parseSingleToken (const String& text)
    {
        if (text.isEmpty())
            return CPlusPlusCodeTokeniser::tokenType_error;

        TaurineCTokeniserFunctions::StringIterator i (text);
        i.skipWhitespace();
        const int tok = TaurineCTokeniserFunctions::readNextToken (i);
        i.skipWhitespace();
        i.skip();
        return i.isEOF() ? tok : CPlusPlusCodeTokeniser::tokenType_error;
    }

    static String getIntegerSuffix (const String& s)    { return s.retainCharacters ("lLuU"); }
    static String getFloatSuffix (const String& s)      { return s.retainCharacters ("fF"); }

    static String getReplacementStringInSameFormat (const String& old, double newValue)
    {
        {
            TaurineCTokeniserFunctions::StringIterator i (old);

            if (TaurineCTokeniserFunctions::parseFloatLiteral (i))
            {
                String s (newValue);

                if (! s.containsChar ('.'))
                    s += ".0";

                return s + getFloatSuffix (old);
            }
        }

        return getReplacementStringInSameFormat (old, (int64) newValue);
    }

    static String getReplacementStringInSameFormat (const String& old, int64 newValue)
    {
        {
            TaurineCTokeniserFunctions::StringIterator i (old);

            if (TaurineCTokeniserFunctions::parseHexLiteral (i))
            {
                String s ("0x" + String::toHexString (newValue) + getIntegerSuffix (old));

                if (old.toUpperCase() == old)
                    s = s.toUpperCase();

                return s;
            }
        }

        {
            TaurineCTokeniserFunctions::StringIterator i (old);

            if (TaurineCTokeniserFunctions::parseDecimalLiteral (i))
                return String (newValue) + getIntegerSuffix (old);
        }

        return old;
    }

    // Given a type name which could be a smart pointer or other pointer/ref, this extracts
    // the essential class name of the thing that it points to.
    static String getSignificantClass (String cls)
    {
        int firstAngleBracket = cls.indexOfChar ('<');

        if (firstAngleBracket > 0)
            cls = cls.substring (firstAngleBracket + 1).upToLastOccurrenceOf (">", false, false).trim();

        while (cls.endsWithChar ('*') || cls.endsWithChar ('&'))
            cls = cls.dropLastCharacters (1).trim();

        return cls;
    }

    //==============================================================================
    struct ValidTaurineCIdentifierRestriction  : public TextEditor::InputFilter
    {
        ValidTaurineCIdentifierRestriction (bool allowTemplatesAndNamespaces)
            : className (allowTemplatesAndNamespaces) {}

        String filterNewText (TextEditor& ed, const String& text)
        {
            String allowedChars ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_");
            if (className)
                allowedChars += "<>:";

            if (ed.getHighlightedRegion().getStart() > 0)
                allowedChars += "0123456789";

            String s = text.retainCharacters (allowedChars);

            if (CPlusPlusCodeTokeniser::isReservedKeyword (ed.getText().replaceSection (ed.getHighlightedRegion().getStart(),
                                                                                        ed.getHighlightedRegion().getLength(),
                                                                                        s)))
                return String();

            return s;
        }

        bool className;

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ValidTaurineCIdentifierRestriction)
    };
};

//==============================================================================
/** A simple lexical analyser for parsing and/or syntax colouring of C code.

    @see CodeEditorComponent, CodeDocument
*/
class TaurineCTokeniser final : public CodeTokeniser
{
public:
    TaurineCTokeniser() { }
    ~TaurineCTokeniser() override { }

    /** The token values returned by this tokeniser. */
    enum TokenType
    {
        tokenType_error = 0,
        tokenType_comment,
        tokenType_keyword,
        tokenType_operator,
        tokenType_identifier,
        tokenType_integer,
        tokenType_float,
        tokenType_string,
        tokenType_bracket,
        tokenType_punctuation,
        tokenType_preprocessor
    };

    /** This is a handy method for checking whether a string is a C reserved keyword. */
    static bool isReservedKeyword (const String& token) noexcept
    {
        return TaurineCTokeniserFunctions::isReservedKeyword (token.getCharPointer(), token.length());
    }

    //==============================================================================
    int readNextToken (CodeDocument::Iterator& source) override
    {
        return TaurineCTokeniserFunctions::readNextToken (source);
    }

    CodeEditorComponent::ColourScheme getDefaultColourScheme() override
    {
        CodeEditorComponent::ColourScheme cs;

        cs.types =
        {
            { "Error",             0xffcc0000 },
            { "Comment",           0xff00aa00 },
            { "Keyword",           0xff0000cc },
            { "Operator",          0xff225500 },
            { "Identifier",        0xff000000 },
            { "Integer",           0xff880000 },
            { "Float",             0xff885500 },
            { "String",            0xff990099 },
            { "Bracket",           0xff000055 },
            { "Punctuation",       0xff004400 },
            { "Preprocessor Text", 0xff660000 }
        };

        return cs;
    }

private:
    //==============================================================================
    JUCE_LEAK_DETECTOR (TaurineCTokeniser)
};

#endif //SQUAREPINE_TAURINEC_PARSER_H
